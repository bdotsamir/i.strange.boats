# i.strange.boats

Because I was tired of not having my own file host. GG.

## Stack:
* [Bun](https://bun.sh) - v1.0.8 seems to segfault on aarch64 chips, v1.0.7 is fine.
* [Redis](https://redis.io) - This is to cache the file hashes to their file names.
* Whatever reverse proxy you want. I use [nginx](https://nginx.org).

## Setup
1. Install the required system dependencies above.
2. Create a `.env` file, create key `AUTHORIZATION_TOKEN` and set it equal to whatever string you want to use to authenticate with the server. Only you may use this.

## Usage
Make an HTTP `POST` request to `/upload` with the form data `file` set to the file you want to upload. The response will be a JSON object with the key `path` set to the URL of the file you just uploaded. You can then use this URL to view/download the file. For me, I use [this macOS shortcut](https://www.icloud.com/shortcuts/74cbcbd271fc43c881aeadc4fd748b86) I made.

Returned paths try to figure out the file extension based on the MIME type of the file you uploaded. If it can't figure it out, it defaults to `.txt`.