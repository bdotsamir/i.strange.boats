export function checkAuthenticated(headers: Headers): boolean {
  return headers.has("Authorization")
}

export function checkAuthorized(token: string): boolean {
  return token === Bun.env.AUTHORIZATION_TOKEN
}
