import { Elysia, t } from "elysia";
import { checkAuthenticated, checkAuthorized } from "./auth";

import { hashCheck } from "./redisClient";

const app = new Elysia();

const fileTypeMapping: { [key: string]: string } = {
  // Known image types:
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/gif": "gif",
  "image/svg+xml": "svg",
  "image/webp": "webp",
  "image/bmp": "bmp",
  "image/tiff": "tiff",
  "image/x-icon": "ico",

  // Known video types:
  "video/mpeg": "mpeg",
  "video/webm": "webm",
  "video/ogg": "ogg",
  "video/mp4": "mp4",
  "video/x-msvideo": "avi",
  "video/3gpp": "3gp",
  "video/3gpp2": "3g2",

  // Known audio types:
  "audio/midi": "midi",
  "audio/mpeg": "mp3",
  "audio/webm": "webm",
  "audio/ogg": "ogg",
  "audio/wav": "wav",
  "audio/aac": "aac",
  "audio/wave": "wav",
  "audio/x-wav": "wav",
  
  // Known document types:
  "text/plain": "txt",
  "text/html": "html",
  "text/css": "css",
  "text/javascript": "js",
  "text/yaml": "yml",
  "application/json": "json",
  "application/xml": "xml",
  "application/zip": "zip",
  "application/x-rar-compressed": "rar",
  "application/pdf": "pdf",

  // Executables
  "application/vnd.microsoft.portable-executable": "exe",
  "application/jar-archive": "jar",

  // Office document types:
  "application/vnd.openxmlformats-officedocument.presentationml.presentation": "pptx",
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document": "docx",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx"
}

app.get('/:file', ({ params: { file }}) => {
  const bunFile = Bun.file(`./public/${file}`);
  if (bunFile.size === 0) {
    return { "error": "404 Not Found" };
  }

  return bunFile;
});

app.post('/upload', async ({ body, set }) => {
  const hasher = new Bun.CryptoHasher("md5");

  const incomingMIMEtype = body.file.type.split(';')[0];

  if (body.file.size === 0 || !body.file) {
    set.status = 400;
    console.warn("No file provided or size zero");
    return { "error": "400: No file provided or size zero" };
  }

  if (body.file.size >= 1_000_000_000) { // 1GB
    set.status = 413;
    console.warn("File too large:", body.file.size, "bytes");
    return { "error": "413: File too large" };
  }

  // Sometimes the file type has a ";" attached to it. We don't want that.
  const fileType = fileTypeMapping[incomingMIMEtype];
  if (!fileType) {
    set.status = 400;
    console.warn("Unknown file type:", body.file.type, "| Name:", body.file.name);
    return { "error": `400: Unknown file type "${incomingMIMEtype}"` };
  }

  // Generate a random string to use as the file name.
  // const randomStr = randomString();

  hasher.update(await body.file.arrayBuffer());
  const hash = hasher.digest("base64");

  const hashCheckResult = await hashCheck(hash);

  const fileName = `${hashCheckResult}.${fileType}`;
  
  // If the file doesn't exist yet,
  if (Bun.file(fileName).size === 0) {
    // write it to disk.
    console.log(`Writing new file ${fileName} to disk`);
    Bun.write(`./public/${fileName}`, body.file);
  }

  // Return the path to the file.
  return { "path": fileName };
  // return file;
}, {
  body: t.Object({
    file: t.File()
  }),
  beforeHandle: ({ set, request: { headers } }) => {
    // Ensure body is of type multipart/form-data
    if (!headers.get("content-type")?.includes("multipart/form-data")) {
      set.status = 400;
      return { "error": "400: Content-Type must be multipart/form-data" };
    }
    
    if (!checkAuthenticated(headers)) {
      set.status = 401;
      return { "error": "401: Unauthorized" };
    }

    if (!checkAuthorized(headers.get("authorization")!)) {
      set.status = 403;
      return { "error": "403: Forbidden" };
    }
  }
});

app.listen(13370);
console.log(
  `🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`
);