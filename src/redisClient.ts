import { createClient } from 'redis';

const redis = createClient();
redis.on('error', err => {
  console.error('Redis client error: \n', err);
  if(err.code === 'ECONNREFUSED') {
    console.error('Redis connection refused. Is Redis running?');
    process.exit(1);
  }
});
await redis.connect();
console.log("Redis ping:", await redis.ping());

export default redis;

export async function hashCheck(hash: string): Promise<string | null> {
  const redisHash = await redis.get(hash);
  if (!redisHash) {
    redis.set(hash, randomString());
  }
  return await redis.get(hash);
}

export function randomString(length = 6): string {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;

  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}